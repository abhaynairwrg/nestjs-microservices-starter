import { Body, Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Controller()
export class AppController {
  constructor(@Inject('NOTES_MODULE') private client: ClientProxy) {}

  @Get()
  getHello(@Body() body: string): Observable<string> {
    return this.client.send<string>({ cmd: 'HELLO_MSG' }, body);
  }
}
