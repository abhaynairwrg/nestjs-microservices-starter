import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NotesService } from './notes/notes.service';
import { NotesController } from './notes/notes.controller';

@Module({
  imports: [
    ClientsModule.register([
      { name: 'NOTES_MODULE', transport: Transport.TCP },
    ]),
  ],
  controllers: [AppController, NotesController],
  providers: [AppService, NotesService],
})
export class AppModule {}
