import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';

@Injectable()
export class NotesService {
  constructor(@Inject('NOTES_MODULE') private client: ClientProxy) {}

  create(createNoteDto: CreateNoteDto) {
    return this.client.send({ role: 'note', cmd: 'create' }, createNoteDto);
  }

  findAll() {
    return this.client.send({ role: 'note', cmd: 'find-all' }, {});
  }

  async findOne(id: number) {
    try {
      return await this.client.send({ role: 'note', cmd: 'find-by-id' }, {});
    } catch (error) {
      throw error;
    }
  }

  update(id: number, updateNoteDto: UpdateNoteDto) {
    return `This action updates a #${id} note`;
  }

  remove(id: number) {
    return this.client.send({ role: 'note', cmd: 'delete-by-id' }, {});
  }
}
