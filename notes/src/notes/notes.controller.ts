import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UnprocessableEntityException,
} from '@nestjs/common';
import { NotesService } from './notes.service';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { MessagePattern } from '@nestjs/microservices';

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @MessagePattern({ role: 'note', cmd: 'create' })
  create(@Body() createNoteDto: CreateNoteDto) {
    return this.notesService.create(createNoteDto);
  }

  @MessagePattern({ role: 'note', cmd: 'find-all' })
  async findAll() {
    return await this.notesService.findAll();
  }

  @MessagePattern({ role: 'note', cmd: 'find-by-id' })
  findOne(@Param('id') id: string) {
    return this.notesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateNoteDto: UpdateNoteDto) {
    try {
      if (JSON.stringify(updateNoteDto) === '{}') {
        throw new UnprocessableEntityException();
      }

      return this.notesService.update(+id, updateNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @MessagePattern({ role: 'note', cmd: 'delete-by-id' })
  remove(@Param('id') id: string) {
    return this.notesService.remove(+id);
  }
}
