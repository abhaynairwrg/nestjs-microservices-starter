import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { Note } from './entities/note.entity';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Note) private _notesRepository: Repository<Note>,
  ) {}

  create(createNoteDto: CreateNoteDto) {
    const newNote = this._notesRepository.create(createNoteDto);

    return this._notesRepository.save(newNote);
  }

  findAll(): Promise<Note[]> {
    return this._notesRepository.find();
  }

  async findOne(id: number): Promise<Note> {
    try {
      const note = await this._notesRepository.findOneOrFail(id);

      return note;
    } catch (error) {
      throw new NotFoundException();
    }
  }

  async update(id: number, updateNoteDto: UpdateNoteDto): Promise<Note> {
    try {
      const note = await this.findOne(id);

      const updatedNote = {
        ...note,
        ...updateNoteDto,
      };

      return this._notesRepository.save(updatedNote);
    } catch (error) {
      throw error;
    }
  }

  async remove(id: number): Promise<Note> {
    try {
      const note = await this.findOne(id);

      return this._notesRepository.remove(note);
    } catch (error) {
      throw error;
    }
  }
}
